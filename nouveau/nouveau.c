/*
 * Copyright 2012 Red Hat Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Ben Skeggs
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <xf86drm.h>
#include <sys/mman.h>

#include "libdrm_lists.h"
#include "libdrm_atomics.h"
#include "libdrm_macros.h"
#include "nouveau_drm.h"
#include "nvidia.h"
#include "nvidia_drm.h"
#include "private.h"

#include "nvif/class.h"
#include "nvif/cl0080.h"
#include "nvif/ioctl.h"
#include "nvif/unpack.h"

#define HALALBAE
#define CHIPSET_TURING

/* XXX: This needs to be defined for some reason...im missing something */
static void
nouveau_bo_make_global(struct nouveau_bo_priv *nvbo);

#define DEBUG

#ifdef DEBUG
#	define TRACE(x...) printf("nouveau: " x)
#	define CALLED() TRACE("CALLED: %s\n", __PRETTY_FUNCTION__)
#else
#	define TRACE(x...)
# define CALLED()
#endif

/* Unused
int
nouveau_object_mthd(struct nouveau_object *obj,
		    uint32_t mthd, void *data, uint32_t size)
{
	return 0;
}
*/

/* Unused
void
nouveau_object_sclass_put(struct nouveau_sclass **psclass)
{
}
*/

/* Unused
int
nouveau_object_sclass_get(struct nouveau_object *obj,
			  struct nouveau_sclass **psclass)
{
	return 0;
}
*/

drm_public int
nouveau_object_mclass(struct nouveau_object *obj,
		      const struct nouveau_mclass *mclass)
{
  // TODO: Only used for VP3 firmware upload
	CALLED();
	return 0;
}

/* NVGPU_IOCTL_CHANNEL_ALLOC_OBJ_CTX */
drm_public int
nouveau_object_new(struct nouveau_object *parent, uint64_t handle,
		   uint32_t oclass, void *data, uint32_t length,
		   struct nouveau_object **pobj)
{
	struct nouveau_object *obj;
	CALLED();

	if (!(obj = calloc(1, sizeof(*obj))))
		return -ENOMEM;

	if (oclass == NOUVEAU_FIFO_CHANNEL_CLASS)
	{
		struct nouveau_fifo *fifo;
		if (!(fifo = calloc(1, sizeof(*fifo)))) {
			free(obj);
			return -ENOMEM;
		}
		fifo->object = parent;
		fifo->channel = 0;
		fifo->pushbuf = 0;
		obj->data = fifo;
		obj->length = sizeof(*fifo);
	}

	obj->parent = parent;
	obj->oclass = oclass;
	*pobj = obj;
	return 0;
}

/* NVGPU_IOCTL_CHANNEL_FREE_OBJ_CTX */
drm_public void
nouveau_object_del(struct nouveau_object **pobj)
{
	CALLED();
	if (!pobj)
		return;

	struct nouveau_object *obj = *pobj;
	if (!obj)
		return;

	if (obj->data)
		free(obj->data);
	free(obj);
	*pobj = NULL;
}

drm_public void
nouveau_drm_del(struct nouveau_drm **pdrm)
{
	CALLED();
	free(*pdrm);
	*pdrm = NULL;
}

drm_public int
nouveau_drm_new(int fd, struct nouveau_drm **pdrm)
{

	struct nouveau_drm *drm;
	drmVersionPtr ver;

	/* debug_init(); XXX: We arent going to implement this */

	if (!(drm = calloc(1, sizeof(*drm))))
		return -ENOMEM;
	drm->fd = fd;

	if (!(ver = drmGetVersion(fd))) {
		nouveau_drm_del(&drm);
		return -EINVAL;
	}
	*pdrm = drm;

	drm->version = (ver->version_major << 24) |
		       (ver->version_minor << 8) |
		        ver->version_patchlevel;
	drm->nvif = (drm->version >= 0x01000301);
	drmFreeVersion(ver);
	return 0;
}




drm_public int
nouveau_device_new(struct nouveau_object *parent, int32_t oclass,
		   void *data, uint32_t size, struct nouveau_device **pdev)
{
	CALLED();
	struct nv_device_info_v0 info = {};
	union {
		struct nv_device_v0 v0;
	} *args = data;
	uint32_t argc = size;
	struct nouveau_drm *drm = nouveau_drm(parent);
	struct nouveau_device_priv *nvdev;
	struct nouveau_device *dev;
	uint64_t v;
	char *tmp;
	int ret = -ENOSYS;
	#ifdef HALALBAE
	int fd = drmOpenWithType("nouveau", NULL, DRM_NODE_RENDER);
	#else
	int fd = drmOpenWithType("nvidia", NULL, DRM_NODE_RENDER);
	#endif

	if (oclass != NV_DEVICE ||
	    nvif_unpack(ret, &data, &size, args->v0, 0, 0, false))
		return ret;

	if (!(nvdev = calloc(1, sizeof(*nvdev))))
		return -ENOMEM;
	dev = *pdev = &nvdev->base;
	dev->fd = -1;

	if (drm->nvif) {
		info.version = 0;
		info.chipset = nvidia_get_chipid(fd);

		nvdev->base.chipset = info.chipset;
		nvdev->base.lib_version = info.chipset;
		nvdev->have_bo_usage = true;
	} else
	if (args->v0.device == ~0ULL) {
		nvdev->base.object.parent = &drm->client;
		nvdev->base.object.handle = ~0ULL;
		nvdev->base.object.oclass = NOUVEAU_DEVICE_CLASS;
		nvdev->base.object.length = ~0;

		ret = nouveau_getparam(dev, NOUVEAU_GETPARAM_CHIPSET_ID, &v);
		if (ret)
			goto done;
		nvdev->base.chipset = nvidia_get_chipid(fd);

		ret = nouveau_getparam(dev, NOUVEAU_GETPARAM_HAS_BO_USAGE, &v);
		if (ret == 0)
			nvdev->have_bo_usage = (v != 0);
	} else
		return -ENOSYS;

	nvdev->base.vram_size = 256 * 1024 * 1024;

	nvdev->base.gart_size = 512 * 1024 * 1024;

	tmp = getenv("NOUVEAU_LIBDRM_VRAM_LIMIT_PERCENT");
	if (tmp)
		nvdev->vram_limit_percent = atoi(tmp);
	else
		nvdev->vram_limit_percent = 80;

	nvdev->base.vram_limit =
		(nvdev->base.vram_size * nvdev->vram_limit_percent) / 100;

	tmp = getenv("NOUVEAU_LIBDRM_GART_LIMIT_PERCENT");
	if (tmp)
		nvdev->gart_limit_percent = atoi(tmp);
	else
		nvdev->gart_limit_percent = 80;

	nvdev->base.gart_limit =
		(nvdev->base.gart_size * nvdev->gart_limit_percent) / 100;

	ret = pthread_mutex_init(&nvdev->lock, NULL);
	DRMINITLISTHEAD(&nvdev->bo_list);
done:
	if (ret)
		nouveau_device_del(pdev);
	return ret;
}

drm_public void
nouveau_device_del(struct nouveau_device **pdev)
{
	CALLED();
	struct nouveau_device_priv *nvdev = nouveau_device(*pdev);
	if (nvdev) {
		free(nvdev->client);
		pthread_mutex_destroy(&nvdev->lock);
		if (nvdev->base.fd >= 0) {
			struct nouveau_drm *drm =
				nouveau_drm(&nvdev->base.object);
			nouveau_drm_del(&drm);
			if (nvdev->close)
				drmClose(nvdev->base.fd);
		}
		free(nvdev);
		*pdev = NULL;
	}
}

drm_public int
nouveau_getparam(struct nouveau_device *dev, uint64_t param, uint64_t *value)
{
        CALLED();
	/* NOUVEAU_GETPARAM_PTIMER_TIME = NVGPU_GPU_IOCTL_GET_GPU_TIME */
	int ret = 0;
	if (param == NOUVEAU_GETPARAM_GRAPH_UNITS)
		*value = (16 << 8) | 4;
	else if (param == NOUVEAU_GETPARAM_PCI_DEVICE)
		*value = 0; // dummy
	else if (param == NOUVEAU_GETPARAM_HAS_BO_USAGE)
		*value = 1; // dummy to return with bo usage
	else if (param == NOUVEAU_GETPARAM_CHIPSET_ID) {
		printf("HARAMBAE WARN: CALLED NOUVEAU_GETPARAM_CHIPSET_ID");
		*value = 352; // dummy, please define this in code
	} else
		ret = -EINVAL;
	return ret;
}

/* Unused
int
nouveau_setparam(struct nouveau_device *dev, uint64_t param, uint64_t value)
{
	return 0;
}
*/

drm_public int
nouveau_client_new(struct nouveau_device *dev, struct nouveau_client **pclient)
{

	struct nouveau_device_priv *nvdev = nouveau_device(dev);
	struct nouveau_client_priv *pcli;
	int id = 0, i, ret = -ENOMEM;
	uint32_t *clients;
	CALLED();

	pthread_mutex_lock(&nvdev->lock);

	for (i = 0; i < nvdev->nr_client; i++) {
		id = ffs(nvdev->client[i]) - 1;
		if (id >= 0)
			goto out;
	}

	clients = realloc(nvdev->client, sizeof(uint32_t) * (i + 1));
	if (!clients)
		goto unlock;
	nvdev->client = clients;
	nvdev->client[i] = 0;
	nvdev->nr_client++;

out:
	pcli = calloc(1, sizeof(*pcli));
	if (pcli) {
		nvdev->client[i] |= (1 << id);
		pcli->base.device = dev;
		pcli->base.id = (i * 32) + id;
		ret = 0;
	}

	*pclient = &pcli->base;

unlock:
	pthread_mutex_unlock(&nvdev->lock);
	return ret;
}

drm_public void
nouveau_client_del(struct nouveau_client **pclient)
{
        CALLED();
        struct nouveau_client_priv *pcli = nouveau_client(*pclient);
        struct nouveau_device_priv *nvdev;
        if (pcli) {
                int id = pcli->base.id;
                nvdev = nouveau_device(pcli->base.device);
                pthread_mutex_lock(&nvdev->lock);
                nvdev->client[id / 32] &= ~(1 << (id % 32));
                pthread_mutex_unlock(&nvdev->lock);
                free(pcli->kref);
                free(pcli);
        }
}

drm_public static int
nouveau_bo_fence_wait(struct nouveau_bo *bo, uint32_t access)
{
	CALLED();
	
	struct nouveau_bo_priv *nvbo = nouveau_bo(bo);
	int ret = 0;

	/* NOTE: We dont need/can use any of this
	if (nvbo->fence.id >= 0) {
		
		TRACE("waiting on fence {%d,%u}\n", (int)nvbo->fence.id, nvbo->fence.value);
		Result res = nvFenceWait(&nvbo->fence, (access & NOUVEAU_BO_NOBLOCK) ? 0 : -1);
		if (R_FAILED(res))
			ret = -EAGAIN;
		else {
		
			// Reset the fence since we're done with it.
			nvbo->fence.id = -1;
			nvbo->fence.value = 0;

			// TODO: Check for NOUVEAU_BO_WR - maybe we're supposed to flush cache?
		 } 
	}

	*/

	if (ret == 0)
		nvbo->access = 0;
	return ret;

	nvbo->access = 0;
	return ret;
}

drm_public static void
nouveau_bo_del(struct nouveau_bo *bo)
{
	CALLED();
        struct nouveau_drm *drm = nouveau_drm(&bo->device->object);
        struct nouveau_device_priv *nvdev = nouveau_device(bo->device);
        struct nouveau_bo_priv *nvbo = nouveau_bo(bo);

        if (nvbo->head.next) {
                pthread_mutex_lock(&nvdev->lock);
                if (atomic_read(&nvbo->refcnt) == 0) {
                        DRMLISTDEL(&nvbo->head);
                        /*
                         * This bo has to be closed with the lock held because
                         * gem handles are not refcounted. If a shared bo is
                         * closed and re-opened in another thread a race
                         * against DRM_IOCTL_GEM_OPEN or drmPrimeFDToHandle
                         * might cause the bo to be closed accidentally while
                         * re-importing.
                         */
                        drmCloseBufferHandle(drm->fd, bo->handle);
                }
                pthread_mutex_unlock(&nvdev->lock);
        } else {
                drmCloseBufferHandle(drm->fd, bo->handle);
        }
        if (bo->map)
                drm_munmap(bo->map, bo->size);
        free(nvbo);
}

drm_public int
nouveau_bo_new(struct nouveau_device *dev, uint32_t flags, uint32_t align,
	       uint64_t size, union nouveau_bo_config *config,
	       struct nouveau_bo **pbo)
{

	CALLED();
	struct nouveau_device_priv *nvdev = nouveau_device(dev);

	#ifdef HALALBAE
	return 0;
	#endif

	struct nouveau_bo_priv *nvbo = calloc(1, sizeof(*nvbo));
	struct nouveau_bo *bo = &nvbo->base;
	int ret = 0;

	if (align < 0x1000)
		align = 0x1000;
	size = (size + 0xFFF) &~ 0xFFF;

	if (!nvbo)
		return -ENOMEM;
/*  XXX: Proper flag implementation and memory mapping.
	TRACE("Allocating BO of size %ld, align %d, flags 0x%x and kind 0x%x\n", size, align, flags, kind);
	void* mem = memalign(0x1000, size);
	if (!mem)
	{
		TRACE("Out of memory\n");
		free(nvbo);
		return -ENOMEM;
	}

*/
/*
	rc = nvMapCreate(&nvbo->map, mem, size, align, kind, false);
	if (R_FAILED(rc))
	{
		TRACE("Failed to create nvmap object (%x)\n", rc);
		free(mem);
		free(nvbo);
		return -rc;
	}

	rc = nvAddressSpaceMap(&nvdev->addr_space, nvMapGetHandle(&nvbo->map), !(flags & NOUVEAU_BO_COHERENT), kind, &bo->offset);
	if (R_FAILED(rc))
	{
		TRACE("Failed to map object to address space (%x)\n", rc);
		nvMapClose(&nvbo->map);
		free(mem);
		free(nvbo);
		return -rc;
	}
*/
	struct nvidia_gem_alloc_nvkms_memory_params params;

	params.memory_size = size;
	params.block_linear = flags & NOUVEAU_BO_COHERENT;
	params.compressible = !(flags & NOUVEAU_BO_COHERENT);
	ret = nvidia_gem_alloc_nvkms_memory(nvdev->base.fd, &params);
	if (ret < 0)
		return ret;

	atomic_set(&nvbo->refcnt, 1);
	bo->device = dev;
	bo->handle = params.handle;
	bo->size = size;
	bo->flags = flags;
	nvbo->map_handle = nvidia_get_gem_map_offset(nvdev->base.fd, params.handle);
	memset((void *)nvbo->map_handle, 0, bo->size);

	if (config)
		bo->config = *config;
	*pbo = bo;
	return 0;
}

drm_public int
nouveau_device_wrap(int fd, int close, struct nouveau_device **pdev)
{
        CALLED();

	struct nouveau_drm *drm;
	struct nouveau_device_priv *nvdev;
	int ret;

	ret = nouveau_drm_new(fd, &drm);
	if (ret)
		return ret;
	drm->nvif = false;

	ret = nouveau_device_new(&drm->client, NV_DEVICE,
				 &(struct nv_device_v0) {
					.device = ~0ULL,
				 }, sizeof(struct nv_device_v0), pdev);
	if (ret) {
		nouveau_drm_del(&drm);
		return ret;
	}

	nvdev = nouveau_device(*pdev);
	nvdev->base.fd = drm->fd;
	nvdev->base.drm_version = drm->version;
	nvdev->close = close;
	return 0;
}

drm_public static void
nouveau_bo_make_global(struct nouveau_bo_priv *nvbo)
{
	CALLED();
        if (!nvbo->head.next) {
                struct nouveau_device_priv *nvdev = nouveau_device(nvbo->base.device);
                pthread_mutex_lock(&nvdev->lock);
                if (!nvbo->head.next)
                        DRMLISTADD(&nvbo->head, &nvdev->bo_list);
                pthread_mutex_unlock(&nvdev->lock);
        }
}

drm_public int
nouveau_bo_wrap(struct nouveau_device *dev, uint32_t handle,
		struct nouveau_bo **pbo)
{
	// NV30-only
	CALLED();
	return 0;
}

drm_public static int
nouveau_bo_wrap_locked(struct nouveau_device *dev, uint32_t handle,
                       struct nouveau_bo **pbo, int name)
{
	CALLED();
        struct nouveau_drm *drm = nouveau_drm(&dev->object);
        struct nouveau_device_priv *nvdev = nouveau_device(dev);
        struct drm_nouveau_gem_info req = { .handle = handle };
        struct nouveau_bo_priv *nvbo;
        int ret;

        DRMLISTFOREACHENTRY(nvbo, &nvdev->bo_list, head) {
                if (nvbo->base.handle == handle) {
                        if (atomic_inc_return(&nvbo->refcnt) == 1) {
                                /*
                                 * Uh oh, this bo is dead and someone else
                                 * will free it, but because refcnt is
                                 * now non-zero fortunately they won't
                                 * call the ioctl to close the bo.
                                 *
                                 * Remove this bo from the list so other
                                 * calls to nouveau_bo_wrap_locked will
                                 * see our replacement nvbo.
                                 */
                                DRMLISTDEL(&nvbo->head);
                                if (!name)
                                        name = nvbo->name;
                                break;
                        }

                        *pbo = &nvbo->base;
                        return 0;
                }
        }

        ret = drmCommandWriteRead(drm->fd, DRM_NOUVEAU_GEM_INFO,
                                  &req, sizeof(req));
        if (ret)
                return ret;

        nvbo = calloc(1, sizeof(*nvbo));
        if (nvbo) {
                atomic_set(&nvbo->refcnt, 1);
                nvbo->base.device = dev;
                abi16_bo_info(&nvbo->base, &req);
                nvbo->name = name;
                DRMLISTADD(&nvbo->head, &nvdev->bo_list);
                *pbo = &nvbo->base;
                return 0;
        }

        return -ENOMEM;
}

drm_public int
nouveau_bo_name_ref(struct nouveau_device *dev, uint32_t name,
		    struct nouveau_bo **pbo)
{
	CALLED();
        struct nouveau_drm *drm = nouveau_drm(&dev->object);
        struct nouveau_device_priv *nvdev = nouveau_device(dev);
        struct nouveau_bo_priv *nvbo;
        struct drm_gem_open req = { .name = name };
        int ret;

        pthread_mutex_lock(&nvdev->lock);
        DRMLISTFOREACHENTRY(nvbo, &nvdev->bo_list, head) {
                if (nvbo->name == name) {
                        ret = nouveau_bo_wrap_locked(dev, nvbo->base.handle,
                                                     pbo, name);
                        pthread_mutex_unlock(&nvdev->lock);
                        return ret;
                }
        }

        ret = drmIoctl(drm->fd, DRM_IOCTL_GEM_OPEN, &req);
        if (ret == 0) {
                ret = nouveau_bo_wrap_locked(dev, req.handle, pbo, name);
        }

        pthread_mutex_unlock(&nvdev->lock);
        return ret;
}

drm_public int
nouveau_bo_name_get(struct nouveau_bo *bo, uint32_t *name)
{
        CALLED();

        struct drm_gem_flink req = { .handle = bo->handle };
        struct nouveau_drm *drm = nouveau_drm(&bo->device->object);
        struct nouveau_bo_priv *nvbo = nouveau_bo(bo);

        *name = nvbo->name;
        if (!*name) {
                int ret = drmIoctl(drm->fd, DRM_IOCTL_GEM_FLINK, &req);

                if (ret) {
                        *name = 0;
                        return ret;
                }
                nvbo->name = *name = req.name;

                nouveau_bo_make_global(nvbo);
        }
        return 0;
}

drm_public void
nouveau_bo_ref(struct nouveau_bo *bo, struct nouveau_bo **pref)
{
	CALLED();
	struct nouveau_bo *ref = *pref;
	if (bo) {
		atomic_inc(&nouveau_bo(bo)->refcnt);
	}
	if (ref) {
		if (atomic_dec_and_test(&nouveau_bo(ref)->refcnt))
			nouveau_bo_del(ref);
	}
	*pref = bo;
}

drm_public int
nouveau_bo_prime_handle_ref(struct nouveau_device *dev, int prime_fd,
			    struct nouveau_bo **bo)
{
	CALLED();
	return -ENOSYS;
}

drm_public int
nouveau_bo_set_prime(struct nouveau_bo *bo, int *prime_fd)
{
	CALLED();
	return -ENOSYS;
}
/* XXX: Dead?
int
nouveau_bo_get_syncpoint(struct nouveau_bo *bo, unsigned int *out_threshold)
{
	CALLED();
	struct nouveau_bo_priv *nvbo = nouveau_bo(bo);

	if (out_threshold)
		*out_threshold = nvbo->fence.value;

	return nvbo->fence.id;
}
*/
drm_public int
nouveau_bo_wait(struct nouveau_bo *bo, uint32_t access,
		struct nouveau_client *client)
{
	CALLED();
	struct nouveau_bo_priv *nvbo = nouveau_bo(bo);
	struct nouveau_pushbuf *push;

	if (!(access & NOUVEAU_BO_RDWR))
		return 0;

	push = cli_push_get(client, bo);
	if (push && push->channel)
		nouveau_pushbuf_kick(push, push->channel);

	if (!(nvbo->access & NOUVEAU_BO_WR) && !(access & NOUVEAU_BO_WR))
		return 0;

	return nouveau_bo_fence_wait(bo, access);
}

drm_public int
nouveau_bo_map(struct nouveau_bo *bo, uint32_t access,
               struct nouveau_client *client)
{
        CALLED();

        struct nouveau_drm *drm = nouveau_drm(&bo->device->object);
        struct nouveau_bo_priv *nvbo = nouveau_bo(bo);
        if (bo->map == NULL) {
                bo->map = drm_mmap(0, bo->size, PROT_READ | PROT_WRITE,
                               MAP_SHARED, drm->fd, nvbo->map_handle);
                if (bo->map == MAP_FAILED) {
                        bo->map = NULL;
                        return -errno;
                }
        }
        return nouveau_bo_wait(bo, access, client);
}


/* XXX: AFAIK: No one uses this, not sure though, maybe remove? */
void
nouveau_bo_unmap(struct nouveau_bo *bo)
{
	CALLED();
	bo->map = NULL;
}
